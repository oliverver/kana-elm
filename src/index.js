import('./Main.elm')
    .then(({ Elm }) => {
        Elm.Main.init({
            node: document.getElementById('main')
        });
    });
