import Browser
import Debug
import Html
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List.Extra exposing (getAt, removeAt)
import Random exposing (Generator)
import Random.List exposing (shuffle)

import Kana exposing (..)

main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }

type alias Model =
  { kana: List Kana
  , guess: Maybe Int
  }

init : () -> (Model, Cmd Msg)
init _ =
  ( Model [] Nothing
  , Random.generate SetKana shuffleKana
  )

type Msg
  = SetKana (List Kana)
  | Guess Int
  | Check Mora

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    SetKana k ->
      ( { model | kana = k }
      , Cmd.none
      )
    Guess i ->
      ( { model | guess = Just i }
      , Cmd.none
      )
    Check mora ->
      let
        updatedModel = model.guess
          |> Maybe.andThen (\i -> getAt i model.kana)
          |> Maybe.andThen (\k -> if isMora mora k then model.guess else Nothing)
          |> Maybe.map (\i -> { model | kana = removeAt i model.kana, guess = Nothing })
          |> Maybe.withDefault model
      in
        ( updatedModel
        , Cmd.none
        )

shuffleKana : Generator (List Kana)
shuffleKana =
  let
    splitMora : Maybe Mora -> List Kana
    splitMora maybeMora =
      case maybeMora of
        Just mora ->
          [ (Hiragana mora.hiragana)
          , (Katakana mora.katakana)
          ]
        Nothing -> []
    kana : List Kana
    kana = List.concatMap (List.concatMap splitMora) gojuon
  in
    shuffle kana


subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

view : Model -> Html Msg
view model =
  div [ class "content" ]
    [ moraTable model
    , kanaList model
    ]

moraTable : Model -> Html Msg
moraTable model = table [ class "gojuon" ] (List.map (moraRow model) gojuon)

moraRow : Model -> List (Maybe Mora) -> Html Msg
moraRow model moras = tr [] (List.map (moraCell model) (List.reverse moras))

moraCell : Model -> Maybe Mora -> Html Msg
moraCell model maybeMora =
  case maybeMora of
    Just mora ->
      let
        showSolved : String -> Html.Html msg
        showSolved kana =
          if (List.any (isKana kana) model.kana)
          then text ""
          else text kana
      in
        td [] [ div [ onClick (Check mora), class "mora" ]
          [ div [ class "hiragana" ] [ showSolved mora.hiragana ]
          , div [ class "katakana" ] [ showSolved mora.katakana ]
          , div [ class "romaji" ] [ text mora.romaji ]
          ]
        ]
    Nothing ->
      td [] []

kanaList : Model -> Html Msg
kanaList model =
  let
    attrs c i =
      [ class c
      , if model.guess == Just i then class "guess" else class ""
      , onClick (Guess i)
      ]
    kanaEl i kana =
      case kana of
        Hiragana h -> div (attrs "hiragana" i) [ text h ]
        Katakana k -> div (attrs "katakana" i) [ text k ]
  in
    div [ class "kana" ] (List.indexedMap kanaEl model.kana)