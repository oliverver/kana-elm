module Kana exposing (Mora, Kana(..), gojuon, isKana, isMora)

type alias Mora =
  { hiragana : String
  , katakana : String
  , romaji : String
  }

type Kana
  = Hiragana String
  | Katakana String

isKana : String -> Kana -> Bool
isKana c kana =
  case kana of
    Hiragana h -> c == h
    Katakana k -> c == k

isMora : Mora -> Kana -> Bool
isMora { hiragana, katakana } kana =
  case kana of
    Hiragana h -> hiragana == h
    Katakana k -> katakana == k

gojuon : List (List (Maybe Mora))
gojuon =
  [
    [ Just (Mora "あ" "ア" "a")
    , Just (Mora "か" "カ" "ka")
    , Just (Mora "さ" "サ" "sa")
    , Just (Mora "た" "タ" "ta")
    , Just (Mora "な" "ナ" "na")
    , Just (Mora "は" "ハ" "ha")
    , Just (Mora "ま" "マ" "ma")
    , Just (Mora "や" "ヤ" "ya")
    , Just (Mora "ら" "ラ" "ra")
    , Just (Mora "わ" "ワ" "wa")
    ]
  , [ Just (Mora "い" "イ" "i")
    , Just (Mora "き" "キ" "ki")
    , Just (Mora "し" "シ" "shi")
    , Just (Mora "ち" "チ" "chi")
    , Just (Mora "に" "ニ" "ni")
    , Just (Mora "ひ" "ヒ" "hi")
    , Just (Mora "み" "ミ" "mi")
    , Nothing
    , Just (Mora "り" "リ" "ri")
    , Nothing
    ]
  , [ Just (Mora "う" "ウ" "u")
    , Just (Mora "く" "ク" "ku")
    , Just (Mora "す" "ス" "su")
    , Just (Mora "つ" "ツ" "tsu")
    , Just (Mora "ぬ" "ヌ" "nu")
    , Just (Mora "ふ" "フ" "fu")
    , Just (Mora "む" "ム" "mu")
    , Just (Mora "ゆ" "ユ" "yu")
    , Just (Mora "る" "ル" "ru")
    , Just (Mora "ん" "ン" "n")
    ]
  , [ Just (Mora "え" "エ" "e")
    , Just (Mora "け" "ケ" "ke")
    , Just (Mora "せ" "セ" "se")
    , Just (Mora "て" "テ" "te")
    , Just (Mora "ね" "ネ" "ne")
    , Just (Mora "へ" "ヘ" "he")
    , Just (Mora "め" "メ" "me")
    , Nothing
    , Just (Mora "れ" "レ" "re")
    , Nothing
    ]
  , [ Just (Mora "お" "オ" "o")
    , Just (Mora "こ" "コ" "ko")
    , Just (Mora "そ" "ソ" "so")
    , Just (Mora "と" "ト" "to")
    , Just (Mora "の" "ノ" "no")
    , Just (Mora "ほ" "ホ" "ho")
    , Just (Mora "も" "モ" "mo")
    , Just (Mora "よ" "ヨ" "yo")
    , Just (Mora "ろ" "ロ" "ro")
    , Just (Mora "を" "ヲ" "wo")
    ]
  ]